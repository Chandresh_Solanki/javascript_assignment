//Required Variables
var firstValue = 0;
var secondValue = 0;
var operation = '';  // to store operation symbol
var isFirst = false; // to check user enter second number or not
var isGetResultKeyPress = false; // to check whether user enter "=" button or others like "+", "-", "/", "*";
var isExpressionEvaluted = true;
var isFlagOneSet = false;
var isFlagTwoSet = false;


// #if -- store fisrt value as well as first operation 
// #else -- store second operation 
//isFirst isFlagOneSet decide whether first is stored or not
// #if 1+ else 1+1
function StoreInputAndOperation(currentOperation) {
    if (!isFirst) {
        firstValue = parseFloat(document.form.result.value);
        operation = currentOperation;
        document.form.expression.value += (document.form.result.value + operation);
        document.form.result.value = firstValue.toString();
        isFirst = true;
    }
    else {
        EvaluateExpression(operation);
        operation = currentOperation;
        document.form.expression.value += secondValue.toString() + operation;
        isExpressionEvaluted = true;
    }
}

//it store second value and based on previous operator,
//it evolutate expression like '1 + 1' and store it to first value again 
// and pass current operation to GetResultBasedOnPressedOperator to check whether current 
// operation is '=' or '+','-', '*', '/' 
function EvaluateExpression(previousOperation) {
    secondValue = parseFloat(document.form.result.value);

    switch (previousOperation) {
        case '+': {
            firstValue = parseFloat(firstValue + secondValue);
            GetResultBasedOnPressedOperator(firstValue);
        }
            break;
        case '-': {
            firstValue = parseFloat(firstValue - secondValue);
            GetResultBasedOnPressedOperator(firstValue);
        }
            break;

        case 'x': {
            firstValue = parseFloat(firstValue * secondValue);
            GetResultBasedOnPressedOperator(firstValue);
        }
            break;

        case '÷': {
            if (secondValue == 0) {
                document.form.result.value = 'Cannot Divided By Zero ';
                document.form.expression.value += ' 0';
                return;
            }
            firstValue = parseFloat(firstValue / secondValue);
            GetResultBasedOnPressedOperator(firstValue);
        }
            break;
    }
}

// perform opertion based on operation given by user 
// if '+', '-', '*', '/' else '='
function GetResultBasedOnPressedOperator(firstValue) {
    if (!isGetResultKeyPress) {
        document.form.result.value = firstValue.toString();
    }
    else {
        document.form.result.value = firstValue.toString();
        document.form.expression.value = " ";
    }
}
//to clear value of result and expression text box
// and reset all other application result
function Reset() {
    // console.log('hello');
    document.form.result.value = "0 ";
    document.form.expression.value = " ";
    firstValue = 0;
    secondValue = 0;
    operation = '';
    isFirst = false;
    isGetResultKeyPress = false;
    isExpressionEvaluted = true;
}
//take input and display in result text box.
function Input(number) {

    if (document.form.expression.value != " " && isExpressionEvaluted) {
        document.form.result.value = "";
        isExpressionEvaluted = false;
    }

    if (number == 0) {
        if (document.form.result.value != "0 ")
            if (isFlagTwoSet) {
                document.form.result.value = "";
                document.form.result.value += "0";
                isFlagTwoSet = false;
            }
            else {
                document.form.result.value += "0";
            }
    }

    else if (document.form.result.value == "0 ") {
        document.form.result.value = number.toString();
    }
    else {
        if (isFlagOneSet) {
            document.form.result.value = "";
            document.form.result.value += number.toString();
            isFlagOneSet = false;
        }
        else {
            document.form.result.value += number.toString();
        }
    }
}

//if document.form.result.value does not contain '.' then add else not add
function AddDecimalPoint(decimalPoint) {
    if (!document.form.result.value.includes(decimalPoint))
        document.form.result.value += '.';
}

// to remove last digit of result
function RemoveLastDigit() {
    if (document.form.result.value.length > 1)
        document.form.result.value = document.form.result.value
            .substring(0, document.form.result.value.length - 1);
    else
        document.form.result.value = "0 ";
}

// make postive number negative and positive to negative
function ChangeSignOfResult() {
    var number = parseFloat(document.form.result.value);
    if (number > 0)
        number *= -1;
    else
        number *= -1;
    document.form.result.value = number.toString();
}

// to perform addition
function Add() {
    StoreInputAndOperation('+');
}


// to perform substraction
function Subtract() {
    StoreInputAndOperation('-');
}


// to perform muliplication
function Multiply() {
    StoreInputAndOperation('x');
}



// to perform division
function Divide() {
    StoreInputAndOperation('÷');
}


// show final result of expression.
function GetResult() {
    isGetResultKeyPress = true;
    EvaluateExpression(operation);
    operation = "";
    isGetResultKeyPress = false;
    isFirst = false;
    isFlagOneSet = true;
    isFlagTwoSet = true;
}

//to Change backgroud color
function ChangeBackgroundColor(object) {
    object.style.background = "yellow";
}

//to Display welcome messge 
function DisplayWelcomMessage() {
    alert("We are Happy to searve you :)");
}